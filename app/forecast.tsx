import { View, Text, TextInput, Button, ActivityIndicator, Image, Keyboard } from 'react-native';
import { styles } from './styles/forecastStyles';

interface ForecastCardProps {
    date: string;
    maxTemp: number;
    iconWeather: string;
  }
  
  const ForecastCard: React.FC<ForecastCardProps> = ({ date, maxTemp, iconWeather }) => {
    const day = new Date(date).toLocaleDateString('fr-FR', { weekday: 'long' as const });
    const dayCapitalize = day[0].toUpperCase() + day.slice(1)

    return (
      <View style={styles.container}>
        <Text>{dayCapitalize}</Text>
        <Image style={styles.weatherIcon} source={{ uri: `https:${iconWeather}` }}/>
        <Text>Max: {maxTemp} °C</Text>
      </View>
    );
  };
  
  export default ForecastCard;  