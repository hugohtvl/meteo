import { View, Text, Image } from 'react-native';
import { styles } from './styles/meteoDetailsStyle';
import {  useRouter, useLocalSearchParams } from "expo-router";

export default function MeteoDetails() {
  const router = useRouter();
  const params = useLocalSearchParams();
  const { meteoData } = params;
  const meteoDataString = meteoData ? meteoData.toString() : "";
  const meteoDataJson = JSON.parse(meteoDataString)

  return (
    <View style={styles.globalContainer}>
      <View style={styles.infoContainer}>
        <Image source={require('../assets/images/temp.png')} />
        <Text>Température min : {meteoDataJson.forecast.forecastday[0].day.mintemp_c} °C</Text>
      </View>
      <View style={styles.infoContainer}>
        <Image source={require('../assets/images/temp-max.png')} />
        <Text>Température max : {meteoDataJson.forecast.forecastday[0].day.maxtemp_c} °C</Text>
      </View>
      <View style={styles.infoContainer}>
        <Image source={require('../assets/images/vent.png')} />
        <Text >Vitesse moyenne du vent : {meteoDataJson.current.wind_kph} km/h</Text>
      </View>
      <View style={styles.infoContainer}>
        <Image source={require('../assets/images/vent-max.png')} />
        <Text>Vitesse max du vent : {meteoDataJson.forecast.forecastday[0].day.maxwind_mph} km/h</Text>
      </View>
      <View style={styles.infoContainer}>
        <Image source={require('../assets/images/precipitation.png')} />
        <Text>Précipitations : {meteoDataJson.forecast.forecastday[0].day.totalprecip_mm} mm</Text>
      </View>
      <View style={styles.infoContainer}>
        <Image source={require('../assets/images/humidite-moy.png')} />
        <Text>Humidité moyenne: {meteoDataJson.forecast.forecastday[0].day.avghumidity} %</Text>
      </View>
      <View style={styles.infoContainer}>
        <Image source={require('../assets/images/humidite.png')} />
        <Text>Humidité : {meteoDataJson.current.humidity} %</Text>
      </View>
      <View style={styles.infoContainer}>
        <Image source={require('../assets/images/uv.png')} />
        <Text>Indice UV : {meteoDataJson.forecast.forecastday[0].day.uv}</Text>
      </View>
      <View style={styles.buttonContainer}><Text style={styles.textButton}
        onPress={() => {
          router.push({ pathname: "/", params: {} });
        }}
      >
        Retour
      </Text></View>
    </View>
  );
};