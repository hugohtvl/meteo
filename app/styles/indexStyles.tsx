import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
      padding: 16,
      backgroundColor: 'white',
      height: '100%',
      flexDirection: 'column',
      alignItems: 'center',
    },
    input: {
      height: 40,
      borderColor: 'gray',
      borderWidth: 1,
      marginBottom: 10,
      paddingHorizontal: 8,
      borderRadius: 10,
      width: '100%',
    },
    results: {
      marginTop: 20,
    },
    conditions: {
      marginTop: 30,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: '70%',
    },
    title: {
      fontSize: 15,
      textAlign: 'center',
      marginBottom: 5,
    },
    subTitle: {
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    conditionContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 10,
    },
    weatherIcon: {
      width: 64,
      height: 64,
      marginRight: 10,
    },
    conditionIcon: {
      width: 32,
      height: 32,
    },
    forecast: {
      flexDirection:  'row',
      justifyContent: 'center',
      gap: 10,
    },
    prevision: {
      textAlign: 'center', 
      marginTop: 30, 
      marginBottom: 30,
      fontSize: 20,
    },
    indexButton: {
      marginTop: 30,
      padding: 20,
      backgroundColor: 'orange',
      borderRadius: 10,
      justifyContent: 'center',
      textAlign: 'center',
    },
    textButton: {
      textAlign: 'center',
    },
  });