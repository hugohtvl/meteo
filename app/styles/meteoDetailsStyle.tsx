import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    globalContainer: {
      padding: 16,
      height: '100%',
      gap: 10,
      flexDirection: 'column',
      alignItems: 'center',
    },
    infoContainer: {
     padding: 20,
     backgroundColor: 'lightblue',
     borderRadius: 15,
     width: '100%',
     flexDirection: 'row',
     alignItems: 'center',
     justifyContent: 'space-between',
    },
    buttonContainer: {
      marginTop: 20,
      padding: 20,
      backgroundColor: 'orange',
      borderRadius: 10,
      width: '50%',
    },
    textButton: {
      textAlign: 'center',
    },
  });