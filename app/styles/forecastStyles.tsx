import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
      padding: 16,
      backgroundColor: 'lightblue',
      borderRadius: 10,
      alignItems: 'center',
      height: '100%',
    },
    weatherIcon: {
      width: 64,
      height: 64,
    },
  });