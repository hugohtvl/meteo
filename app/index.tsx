import { View, Text, TextInput, Button, ActivityIndicator, Image, Keyboard, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { MeteoData } from './types';
import { useState, useEffect } from 'react';
import { styles } from './styles/indexStyles';
import ForecastCard from './forecast';
import { useRouter } from 'expo-router';

export default function HomeScreen() {
  const [location, setLocation] = useState<string>('');
  const [meteoData, setMeteoData] = useState<MeteoData>();
  const [loading, setLoading] = useState<boolean>(false);
  const [searchResults, setSearchResults] = useState<Array<any>>([]);
  const [isFirstLoad, setIsFirstLoad] = useState<boolean>(true);
  const apiKey = '93e89c845ed247399f0122001242805';
  const baseUrl = 'http://api.weatherapi.com/v1';
  const router = useRouter();

  const searchCity = async (query: string) => {
    try {
      const response = await fetch(`${baseUrl}/search.json?key=${apiKey}&q=${query}`);
      const json: Array<any> = await response.json();
      setSearchResults(json);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    if (location.length > 2) {
      searchCity(location);
    }
  }, [location]);

  const storeLastCity = async (location: string) => {
    try {
      await AsyncStorage.setItem('lastCity', location);
    } catch (e) {
      console.error(e)
    }
  };

  useEffect(() => {
    const getLastCity = async () => {
      const lastCity = await AsyncStorage.getItem('lastCity');
      if (lastCity !== null) {
        setLocation(lastCity);
      }
    };

    getLastCity();
  }, []);

  useEffect(() => {
    if (isFirstLoad && location) {
      handleSearch();
      setIsFirstLoad(false);
    }
  }, [location]);

  const getMeteoFromApi = async (location: string) => {
    try {
      setLoading(true);
      const response = await fetch(`${baseUrl}/forecast.json?key=${apiKey}&q=${location}&days=3`);
      const json: MeteoData = await response.json();
      setMeteoData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  const handleSearch = () => {
    Keyboard.dismiss();
    getMeteoFromApi(location);
  };

  const images: { [key: string]: any } = {
    vent: require('../assets/images/vent.png'),
    eau: require('../assets/images/eau.png'),
    soleil: require('../assets/images/soleil.png'),
  };

  const data = meteoData && meteoData.current ? [
    { image: 'vent', text: `${meteoData.current.wind_kph} km/h` },
    { image: 'eau', text: `${meteoData.current.humidity} %` },
    { image: 'soleil', text: `${meteoData.forecast.forecastday[0].astro.sunrise}`},
  ] : [];

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Saisissez une ville (partout dans le monde)"
        placeholderTextColor="black"
        onChangeText={setLocation}
        value={location}
      />

      {location && searchResults.map((result, index) => {
        if (result.name.toLowerCase() !== location.toLowerCase()) {
          return (
            <Text
              key={index}
              onPress={() => {
                setLocation(result.name);
              }}
            >
              {result.name}, {result.region}, {result.country}
            </Text>
          )
        }
      })
      }

      {searchResults.some(result => result.name.toLowerCase() === location.toLowerCase()) && (
        <Button title="Rechercher" onPress={() => { handleSearch(); storeLastCity(location); setLocation(''); }} />
      )}

      {loading ? (
        <ActivityIndicator size="large" color="#0000ff" />
      ) : (
        meteoData && meteoData.location && meteoData.current && meteoData.forecast && (
          <>
            <View style={styles.results}>
              <Text style={styles.title}>Météo pour</Text>
              <Text style={styles.subTitle}>{meteoData.location.name}, {meteoData.location.region}, {meteoData.location.country}</Text>
              <View style={styles.conditionContainer}>
                <Image
                  style={styles.weatherIcon}
                  source={{ uri: `https:${meteoData.current.condition.icon}` }}
                />
                <Text style={{ fontSize: 30 }}>{meteoData.current.temp_c}°C</Text>
              </View>
            </View>
            {data?.map((item, index) => (
              <View key={index} style={styles.conditions}>
                <Image source={images[item.image]} style={styles.conditionIcon} />
                <Text>{item.text}</Text>
              </View>
            ))}
            <Text style={styles.prevision}>Prévision des 3 prochains jours :</Text>
            <View style={styles.forecast}>
              {meteoData.forecast.forecastday.map((day, index) => (
                <ForecastCard
                  key={index}
                  date={day.date}
                  maxTemp={day.day.maxtemp_c}
                  iconWeather={day.day.condition.icon}
                />
              ))}
            </View>
            <View style={styles.indexButton}>
            <Text
              style={styles.textButton}
              onPress={() => {
                router.push({
                  pathname: '/meteoDetails',
                  params: { meteoData: JSON.stringify(meteoData) },
                });
              }}
            >
              Voir plus de détails
            </Text>
          </View>
          </>
        )
      )}
    </View>
  );
};